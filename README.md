# Freeciv Log Graph

This script is to generate png files from [Freeciv](http://www.freeciv.org/) logs.

## Dependencies

*   Python (haha)
*   GnuPlot 
*   NumPy

## Usage

`./fclog.py -l myempire.log -t template.gpl [-d] [-h]`

The debug option prints the gnuplot data and commands to the screen 
instead of piping it into gnuplot.

The defaults are the standard freeciv log filename and the provided 
template.txt, so if both files are in place just run the script.

The template are common gunplot commands so if you want anything fancy
edit as you wish.

## Author

Gergely Tomka, tomkatudor@gmail.com

## License

[WTFPL](http://www.wtfpl.net/)
