#!/usr/bin/python3

import numpy
import string
import subprocess
import argparse

parser = argparse.ArgumentParser(description='A script to draw pngs from the freeciv stat log.')
parser.add_argument('-l', '--log', help='Freeciv stat log file', default='freeciv-score.log')
parser.add_argument('-t', '--template', help='Template file', default='template.txt')
parser.add_argument('-d', '--debug', help='Fills the screen with garbage.', action='store_true')
args = parser.parse_args()

#should we want to debug we just spew the whole big
#gnuplot file to the stdout.
if args.debug:
    command = '/bin/cat'
else:
    command = '/usr/bin/gnuplot'

#slurp in the template
with open(args.template, 'r') as t:
    template = string.Template(t.read())

players = {}
tags = []
turnst = []
turnsy = []
content = []

#slurp in the data
with open(args.log, 'r') as f:
    for line in f:
        if line.startswith('data'):
            content.append(line.rstrip())
        elif line.startswith('turn'):
            turnsy.append(line.rstrip().split(' ', 3)[2])
            turnst.append(line.rstrip().split(' ', 3)[1])
        elif line.startswith('tag'):
            tags.append(line.rstrip().split(' ', 2)[2])
        elif line.startswith('addplayer'):
            playeridx=int(line.rstrip().split(' ', 3)[2])
            players[playeridx]=line.rstrip().split(' ', 3)[3]

if args.debug:
    print('Collected tags')
    print(players)
    print(tags)
    print(turnst)
    print(turnsy)
    print(max(players))

#The data line is like this:
# data turn tag player value
#But we need it in a 3d table like this:
# data[tag][turn][player]
#so we have to rearrange these numbers, this is why we have to
#load the info then write it out.
data = numpy.zeros([len(tags)+1, len(turnst)+1, max(players)+1], dtype=numpy.int)

#rearrange in the data.
turnn = -1
actual_turn = ''
for line in content:
    word, turn, tag, player, value = line.split(' ')
    if actual_turn != turn :
        turnn = turnn+1
        actual_turn = turn
    data[int(tag)][turnn][int(player)] = int(value)

#one gnuplot instance to rule them all.
gpl = subprocess.Popen(command, stdin=subprocess.PIPE, encoding='utf8')
for graphtype in ['turns', 'years']:
    for tag, tagname in enumerate(tags):
        #writing the header from a template
        subs = {'TAG':tagname, 'TYPE':graphtype}
        gpl.stdin.write(template.substitute(subs))
        #dumping the data
        for turn, turnname in enumerate(turnsy):
            if graphtype == 'turns':
                gpl.stdin.write(turnst[turn]+' ')
            else:
                gpl.stdin.write(turnsy[turn]+' ')
            for player, playername in iter(players.items()):
                gpl.stdin.write(str(data[tag][turn][player])+' ')
            gpl.stdin.write('\n')
        #writing the actual plot command
        gpl.stdin.write('EOD\nplot\\\n')
        playeridx=0
        for player, playername in iter(players.items()):
            if player > 0:
                gpl.stdin.write(',\\\n')
            gpl.stdin.write('"$MYDATA" using 1:{} with lines lw 3 title "{}"'.format(str(playeridx+2), playername))
            playeridx = playeridx +1
        gpl.stdin.write('\n')
gpl.communicate('')

